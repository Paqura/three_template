import * as THREE from 'three';

function init() {
  /**
   * * Контейнер куда свалится канвас
   */
  const app = document.querySelector('.app');

  /**
   * * Сцена - место, куда будут добавляться объекты
   */

  const scene = new THREE.Scene();

  /**
   * * Камера @param 
   * * {number} вертикальный угол обзора;
   * * {number} отношение горизонтали к вертикали(не менять)   
   * * {number} мы видим объект, если он на расстояние не меньше 0.1px
   * * {number} мы видим объект, если он на расстояние не больше 5000px
   */

  const camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 1000);

  /**
   * * Положение камеры @param
   * * {number} x
   * * {number} y
   * * {number} z
   */

  camera.position.set(0, 0, 5);

  /**
   * * Рендерер - принимает сцену и камеру, и рендерит
   */

  const renderer = new THREE.WebGLRenderer();
  renderer.setSize(window.innerWidth, window.innerHeight);

  checkAppChilds();

  /**
   * * Любой объект в THREEjs мире (mesh)
   * * состоит из геометрии и материала
   */

  const cubeGeometry = new THREE.BoxGeometry(2, 2, 2);
  const cubeMaterial = new THREE.MeshBasicMaterial({
    color: 0x00ff00,
    wireframe: true
  });

  const circleGeometry = new THREE.CircleGeometry(2, 128);
  const circleMaterial = new THREE.MeshBasicMaterial({
    color: 0xffffff,
    wireframe: true
  });

  const circle = new THREE.Mesh(circleGeometry, circleMaterial);
  const cube = new THREE.Mesh(cubeGeometry, cubeMaterial);
  scene.add(circle);
  scene.add(cube);

  /**
   * * Функция, проверяющая есть ли внутри 
   * * контейнера app другие теги канваса
   */

  function checkAppChilds() {
    if (app.childNodes.length) {
      app.removeChild(app.firstChild);
      app.appendChild(renderer.domElement);
    } else {
      app.appendChild(renderer.domElement);
    }
  }

  /**
   * * Запуск анимации
   * * в момент запуска функции 
   * * рендерер перерисовывает сцену
   */

  function animate() {
    requestAnimationFrame(animate);

    cube.rotation.y += 0.02;
    cube.rotation.z += 0.02;
    cube.position.z -= 0.05;
    renderer.render(scene, camera);
  }
  animate();
}

/**
 * * Реинициализация при ресайзе
 */

init();

window.addEventListener('resize', init);